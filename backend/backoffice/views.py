from accounts.models import BoardMember, Firefighter, Profile
from django.shortcuts import render, redirect
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.views.generic import TemplateView
from django.db import transaction
from django.contrib.auth.models import User
from siab.functions import get_or_none

class Main(TemplateView):
    template_name = 'backoffice_main.html'

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name, {
        })

class CreateUser(TemplateView):
    template_name = 'create_user.html'

    def get(self, request, *args, **kwargs):
        profiles = Profile.objects.filter(user__isnull = True)
        return render(request, self.template_name, {
            'profiles': profiles
        })

class ProfilesListView(PermissionRequiredMixin,TemplateView):
    permission_required =["accounts.add_profile", "accounts.change_profile"] 
    template_name = 'profiles_list.html'

    def get(self, request, *args, **kwargs):
        profiles = Profile.objects.all()
        return render(request, self.template_name, {
            'profiles': profiles
        })

class ProfileView(PermissionRequiredMixin, TemplateView):
    permission_required =["accounts.add_profile", "accounts.change_profile"] 
    template_name = 'profile_form.html'

    def get(self, request, *args, **kwargs):
        context = {}
        context['document_types'] = Profile.DOCUMENT_TYPES
        if(kwargs['id'] != 0):
            print('edit')
            profile = Profile.objects.get(id=kwargs['id'])
            context['profile'] = profile
            context['firefighter'] = get_or_none(Firefighter, profile=profile)
            context['board_member'] = get_or_none(BoardMember, profile=profile)
        else:
            users_without_profiles = User.objects.filter(profile__isnull=True)
            context['users'] = users_without_profiles
        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        transaction.set_autocommit(False)
        data = request.POST
        try:
            newProfile:Profile = Profile.objects.create(
                first_name = data['first_name'],
                last_name = data['last_name'],
                document_type = data['document_type'],
                document_number = data['document_number'],
                phone = data['phone'],
                address = data['address'],
                birth_date = data['birth_date'],
                image = data['image'],
            )
            if('user' in data and data['user'] != '-1'):
                user:User = User.objects.get(id=data['user'])
                user.first_name = newProfile.first_name
                user.last_name = newProfile.last_name
                user.save()
                newProfile.user = user
            if(data['profile_type'] == 'firefighter'):
                firefighter:Firefighter = Firefighter.objects.create(
                    rank = data['firefighter_rank'],
                    code = data['firefighter_code'],
                    call_code = data['firefighter_call_code'],
                    entry_date = data['firefighter_entry_date'],
                    profile = newProfile
                )
                if(data['firefighter_leaving_date']):
                    firefighter.leaving_date = data['firefighter_leaving_date']
                firefighter.save()
            if(data['profile_type'] == 'board_member'):
                board_member:BoardMember = BoardMember.objects.create(
                    position = data['board_member_position'],
                    entry_date = data['board_member_entry_date'],
                    profile = newProfile
                )
                if(data['board_member_leaving_date']):
                    board_member.leaving_date = data['board_member_leaving_date']
    
                board_member.save()
    
            newProfile.save()
            transaction.commit()
        except:
            transaction.rollback()
        return redirect('profiles_list')

       
