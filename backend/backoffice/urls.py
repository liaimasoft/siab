"""
URL configuration for siab project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/5.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from .views import Main, CreateUser, ProfileView, ProfilesListView
from django.urls import path
from django.contrib.auth.decorators import login_required

urlpatterns = [
    path('', Main.as_view(), name="backoffice"),
    path('create_user', login_required(CreateUser.as_view()), name="create_user"),
    path('profile/<int:id>', login_required(ProfileView.as_view()), name="profile"),
    path('profiles_list', login_required(ProfilesListView.as_view()), name="profiles_list"),
]
