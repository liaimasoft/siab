const toggleSidebar = () => {
    const sidebar = $('#sidebarMenu')
    if(sidebar.hasClass('sidebar-open')){
        // Close de sidebar --
        sidebar.addClass('sidebar-close')
        sidebar.removeClass('sidebar-open')
    }else if(sidebar.hasClass('sidebar-close')){
        // Open de sidebar --
        sidebar.addClass('sidebar-open')
        sidebar.removeClass('sidebar-close')
    }
}

const setValidationForm = () => {
  'use strict'
  const forms = document.querySelectorAll('.needs-validation')

  Array.from(forms).forEach(form => {
    form.addEventListener('submit', event => {
      if (!form.checkValidity()) {
        event.preventDefault()
        event.stopPropagation()
      }
      form.classList.add('was-validated')
    }, false)
  })
}

document.addEventListener("DOMContentLoaded", function(event) {
    $('#sidebar-toggler').on('click', toggleSidebar)
});

