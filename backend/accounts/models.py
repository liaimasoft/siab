from django.db import models
from django.contrib.auth.models import User

class Profile(models.Model):
    DOCUMENT_TYPES = (
        ('D', 'DNI'),
        ('P', 'Passaport'),
        ('O', 'Other')
    )
    first_name = models.CharField()
    last_name = models.CharField()
    document_type = models.CharField(max_length=1, choices=DOCUMENT_TYPES)
    document_number = models.CharField(max_length=11, unique=True)
    phone = models.CharField(max_length=30, blank=True, null=True, default=None)
    address = models.CharField(max_length=100, blank=True, null=True, default=None)
    birth_date = models.DateField(blank=False, null=False, default=None)
    image= models.ImageField(upload_to="profile/images")
    user = models.OneToOneField(User, db_column='id_user', on_delete=models.CASCADE, null=True, blank=True)
    
    class Meta:
        db_table ='"accounts"."profile"' 
        verbose_name = "Profile"
        verbose_name_plural = "Profiles"

    def __str__(self) -> str:
        return self.first_name + ' ' + self.last_name

class Firefighter(models.Model):
    rank = models.CharField(max_length=30)
    entry_date = models.DateField(blank=False, null=False, default=None)
    leaving_date = models.DateField(blank=True, null=True, default=None)
    code = models.CharField(max_length=11)
    call_code = models.CharField(max_length=11)
    profile = models.OneToOneField(Profile, db_column='id_profile', on_delete=models.CASCADE)
    
    class Meta:
        db_table ='"accounts"."firefighter"' 
        verbose_name = "Firefighter"
        verbose_name_plural = "Firefighters"

    def __str__(self) -> str:
        return self.rank + ' ' + self.profile.user.last_name

class BoardMember(models.Model):
    position = models.CharField(max_length=30)
    entry_date = models.DateField(blank=False, null=False, default=None)
    leaving_date = models.DateField(blank=True, null=True, default=None)
    profile = models.OneToOneField(Profile, db_column='id_profile', on_delete=models.CASCADE)
    
    class Meta:
        db_table ='"accounts"."board_member"' 
        verbose_name = "BoardMember"
        verbose_name_plural = "BoardMembers"

    def __str__(self) -> str:
        return self.position + ' ' + self.profile.user.last_name
