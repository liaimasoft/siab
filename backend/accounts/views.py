from django.shortcuts import render, redirect
from django.views.generic import TemplateView
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import login, logout, authenticate

def singout(request):
    logout(request)
    return redirect('login')

class Login(TemplateView):
    template_name = 'login.html'

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name, {
            'form': AuthenticationForm,
        })
    
    def post(self, request, *args, **kwargs):
        data = request.POST
        user = authenticate(request, username = data['username'], password = data['password'])
        if user is None:
            return render(request, self.template_name, {
                'form': AuthenticationForm,
                'error': 'Username or password Incorrect',
            })
        login(request, user)
        return redirect('home')


